import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import Api from './services/api';
import Axios from 'axios'
import store from './store'
import  router from '@/routes/router.js'

Vue.prototype.$http = Axios;
const token = localStorage.getItem('token')
if (token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

Vue.config.productionTip = false

Vue.prototype.$api = Api;

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')


