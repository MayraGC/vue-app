import Vue from 'vue'
import Dashboard from '@/views/Dashboard'
import Movies from '@/views/Movies'
import Login from '@/views/Login'
import Shifts from '@/views/Shifts'
import MovieShifts from '@/views/MovieShifts'
import Router from 'vue-router'
import store from '@/store.js'

Vue.use(Router)

let router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard,
            meta: {
                requiresAuth: true
            },
            children: [
                {
                    path: 'movies',
                    name: 'movies',
                    component: Movies
                },
                {
                    path: 'shifts',
                    name: 'shifts',
                    component: Shifts
                },
                {
                    path: 'movies-shifts',
                    name: 'movies-shifts',
                    component: MovieShifts
                }
            ]
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                requiresAuth: false
            }
        }
    ]
})
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next()
            return
        }
        next('/login')
    } else {
        next()
    }
})

export default router