import axios from 'axios';

function headers() {
    const headers = {};
    const token = localStorage.getItem('token');
    if (token) {
        headers['Authorization'] = `Bearer ${token}`;
    }
    headers['Accept'] = "application/json";
    return headers;
}

export default () => {
    return axios.create({
        mode: 'cors',
        baseURL: `http://localhost:8000/api`,
        headers: headers(),
    })
}